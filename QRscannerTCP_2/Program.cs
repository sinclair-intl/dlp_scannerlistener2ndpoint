﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace QRscannerTCP_2
{
    class Program
    {
        MySqlConnection connection;

        static void Main(string[] args)
        {
            Program p1 = new Program();

            //create a return port number from db function - maybe create threads for each reader?

            TcpListener listener = new TcpListener(System.Net.IPAddress.Any, 6900);
            listener.Start();

            Console.WriteLine("Started");

        clientLabel:
            //TcpClient client = listener.AcceptTcpClient();
            while (true)
            //for(int i = 0; i < 10;i++)
            {
                //Console.WriteLine("New while");
                TcpClient client = listener.AcceptTcpClient();

                try
                {
                    //Console.WriteLine("New try");
                    NetworkStream nwStream = client.GetStream();
                    byte[] buffer = new byte[client.ReceiveBufferSize];

                    int byteRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                    string dataReceived = Encoding.ASCII.GetString(buffer, 0, byteRead);
                    Console.WriteLine("Received: " + dataReceived);
                    if (!p1.dbInsert(dataReceived))
                    {
                        Console.WriteLine("Error occurred in writing to the database");
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error occurred in the TCP/IP network stream. Error: \n" + ex.Message);
                    //goto clientLabel;

                    //goto clientLabel;
                }
                client.Close();
                //goto clientLabel;
            }
            //client.Close();
            listener.Stop();
            Console.ReadLine();

        }

        public bool dbInsert(string strEntry)
        {

            try
            {
                connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=Inc0rrect!;Database=dlp_testv1.0");
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    MySqlCommand command = connection.CreateCommand();
                    //command.CommandText = "INSERT INTO dlp_scanner_raw (raw_stream,reader_id,labelling_date) VALUES (\"" + strEntry + "\",\"1\",curdate())";
                    command.CommandText = "INSERT INTO dlp_scanner_raw2 (raw_stream,reader_id,labelling_date) VALUES (\"" + strEntry + "\",\"1\",now())";
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error occurred while inserting the data stream from the reader.\nraw data: " + strEntry + "\nError: \n" + ex.Message);
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine("Unable to establish DB connection for raw data insert into the database.\nraw data: " + strEntry);
                    return false;
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to establish DB connection for raw data insert into the database.\nraw data: " + strEntry);
                return false;
            }

            return true;
        }
    }
}
